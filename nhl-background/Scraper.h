//
//  Scraper.h
//  nhl-background
//
//  Created by Pierre-Marc Airoldi on 2014-06-12.
//  Copyright (c) 2014 Pete App Designs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Scraper : NSObject

+(void)scrape:(void (^)())completion;

@end
