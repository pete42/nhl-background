//
//  main.m
//  nhl-background
//
//  Created by Pierre-Marc Airoldi on 2014-06-12.
//  Copyright (c) 2014 Pete App Designs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scraper.h"

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        [Scraper scrape:^{
            dispatch_semaphore_signal(sema);
            
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    
    return 0;
}

