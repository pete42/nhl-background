//
//  Scraper.m
//  nhl-background
//
//  Created by Pierre-Marc Airoldi on 2014-06-12.
//  Copyright (c) 2014 Pete App Designs. All rights reserved.
//

#import "Scraper.h"
#import <AFNetworking/AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface Scraper ()

@property AFHTTPSessionManager *manager;
@property dispatch_queue_t queue;

@end

@implementation Scraper

-(instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        _queue = dispatch_queue_create([@"com.peteappdesigns.hockey.scrape" UTF8String], DISPATCH_QUEUE_CONCURRENT);
        
        _manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [_manager setCompletionQueue:_queue];
        [_manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];

    }
    
    return self;
}

+(void)scrape:(void (^)())completion {
    
    Scraper *scraper = [Scraper new];
    
    RACSignal *scoreboardSignal = [[self getWithJSONP:scraper.manager forURL:@"http://live.nhle.com/GameData/20132014/2013030414/gc/gcsb.jsonp"] subscribeOn:[RACScheduler scheduler]] ;
    
    RACSignal *boxscoreSignal = [[self getWithJSONP:scraper.manager forURL:@"http://live.nhle.com/GameData/20132014/2013030414/gc/gcbx.jsonp"] subscribeOn:[RACScheduler scheduler]];

    __block NSDictionary *boxscore;
    __block NSDictionary *scoreboard;
    
    [[[RACSignal combineLatest:@[scoreboardSignal, boxscoreSignal] reduce:^id(NSDictionary *sb, NSDictionary *bx){
        
        scoreboard = sb;
        boxscore = bx;
        
        NSLog(@"%@", [scoreboard description]);
        NSLog(@"%@", [boxscore description]);
       
        return nil;
    }] then:^RACSignal *{
        
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
           
            NSLog(@"next task");
            
            [subscriber sendCompleted];
            
            return [RACDisposable disposableWithBlock:^{
                
            }];
        }];
    }] subscribeCompleted:^{
       
        NSLog(@"all done");
        [self retrunCompletion:completion];
    }];
    
//    RACSignal *databaseSignal = [[databaseClient
//                                  fetchObjectsMatchingPredicate:predicate]
//                                 subscribeOn:[RACScheduler scheduler]];
//    
//    RACSignal *fileSignal = [RACSignal startEagerlyWithScheduler:[RACScheduler scheduler] block:^(id<RACSubscriber> subscriber) {
//        NSMutableArray *filesInProgress = [NSMutableArray array];
//        for (NSString *path in files) {
//            [filesInProgress addObject:[NSData dataWithContentsOfFile:path]];
//        }
//        
//        [subscriber sendNext:[filesInProgress copy]];
//        [subscriber sendCompleted];
//    }];
//    
//    [[RACSignal
//      combineLatest:@[ databaseSignal, fileSignal ]
//      reduce:^ id (NSArray *databaseObjects, NSArray *fileContents) {
//          [self finishProcessingDatabaseObjects:databaseObjects fileContents:fileContents];
//          return nil;
//      }]
//     subscribeCompleted:^{
//         NSLog(@"Done processing");
//     }];
}

+(id)getWithJSONP:(AFHTTPSessionManager *)manager forURL:(NSString *)URL {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        __block id returnObject;
        
        [manager GET:URL parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            returnObject = [self JSONFromJSONP:responseObject];
            dispatch_semaphore_signal(sema);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            returnObject = nil;
            dispatch_semaphore_signal(sema);
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
        NSLog(@"done task");
        
        [subscriber sendNext:returnObject];
        [subscriber sendCompleted];
        
        return [RACDisposable disposableWithBlock:^{
            
        }];
    }];

}

+(void)retrunCompletion:(void (^)())completion {
    
    if (completion != nil) {
        completion();
    }
}

+(id)JSONFromJSONP:(id)jsonp {
    
    NSString *responseString = [[NSString alloc] initWithData:jsonp encoding:NSUTF8StringEncoding];
    responseString = [responseString stringByReplacingCharactersInRange:NSMakeRange(0, [responseString rangeOfString:@"("].location) withString:@""];
    responseString = [responseString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
    
    NSError *error;
    id json = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (error) {
        return nil;
    }
    
    else {
        return json;
    }
}

@end
